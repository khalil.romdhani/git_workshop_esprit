package main.java.com.example.kadem_git.Entites;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class DetailsEquipe {
	
	 @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    public Long idDetailsEquipe ;
	    public Integer salle ;
	    public String thematique ;

	@JsonIgnore
	    @OneToOne(mappedBy = "eq")
	    public Equipe equipe;

}


