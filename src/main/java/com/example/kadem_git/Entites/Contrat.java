package com.example.kadem_git.Entites;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Contrat implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public  Long idContrat ;
    @Temporal(TemporalType.DATE)
    public Date dateDebutC ;
    public Date dateFinC ;
    public Specialite specialite ;
    public float montantC ;

    public Boolean archive;


    @ManyToOne(cascade = CascadeType.ALL)
    Etudiant etu;
}