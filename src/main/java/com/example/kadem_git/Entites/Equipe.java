package com.example.kadem_git.Entites;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="equipe")
@FieldDefaults(level = AccessLevel.PRIVATE )
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Equipe implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idEquipe;
    String nomEquipe;
    @Enumerated(EnumType.STRING)
    Niveau niveau;
    @OneToOne
    private DetailEquipe detailEquipe;
    @ManyToMany(mappedBy = "equipes")
    List<Etudiant> etudiants;
    public void afficher(){
        System.out.println("dali");
        System.out.println("triki");

    }

}
