package com.example.kadem_git.Entites;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Etudiant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer idEtudiant ;
    public String prenomE ;
    public String NOmE ;
    public Domaine domaine ;


    @JsonIgnore
    @ManyToOne
    private Departement dp;



    @OneToMany(mappedBy = "etu")

    List<Contrat> c=new ArrayList<>();


    @ManyToMany
    List<Equipe> equipe=new ArrayList<>();

}
