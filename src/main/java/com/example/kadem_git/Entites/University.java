package com.example.kadem_git.Entites;


import java.util.List;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class University {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer idUniveste ;
    public String nomUniversite ;
    
    @OneToMany
    private List<Departement> d ;

}
